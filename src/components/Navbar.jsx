import React from 'react';

const Navbar = () => {
  return (
    <>
      <nav class="navbar navbar-expand-lg " id="navigation">
        <div class="container-fluid">
          <a class="navbar-brand" href="#hero">
            <svg width="100" height="34" viewBox="0 0 100 34" fill="none" xmlns="http://www.w3.org/2000/svg">
              <rect width="100" height="34" fill="#0D28A6" />
            </svg>
          </a>
          <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav ms-auto">
              <li class="nav-item">
                <a class="nav-link" aria-current="page" href="#our_services">
                  Our Services
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#why_us">
                  Why Us
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#testimonial">
                  Testimonial
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#frequently-asked">
                  FAQ
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link btn btn-success" href="">
                  Register
                </a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    </>
  );
};

export default Navbar;
